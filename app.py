from chad_kingdom import create_app


if __name__ == '__main__':
    app = create_app()
    app.run()



'''
# DB

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.Text)
    password = db.Column(db.Text)
    character_id = db.Column(db.Integer, db.ForeignKey('character.id'))
    character = db.relationship('Character', backref = db.backref('user', uselist=False))

class Character(db.Model):
    __tablename__ = 'character'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.Text)

class Location(db.Model):
    __tablename__ = 'location'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.Text)

db.create_all()


# Create mock users and delete the old instances on run
db.session.query(User).delete()
db.session.query(Character).delete()

mason_pw = bcrypt.hashpw(b"jickpassword", bcrypt.gensalt())
mason_user = User(username = "jick", password = mason_pw, character_id = 1)
db.session.add(mason_user)
db.session.commit()
milo_pw = bcrypt.hashpw(b"irondungpassword", bcrypt.gensalt())
milo_user = User(username = "irondung", password = milo_pw, character_id = 2)
db.session.add(milo_user)
db.session.commit()

mason_character = Character(name = "SCRUNG")
db.session.add(mason_character)
db.session.commit()
milo_character = Character(name = "bing bing")
db.session.add(milo_character)
db.session.commit()


# ROUTES

@app.get("/")
def home():
    return render_template("login.html")

@app.post("/auth")
def auth():
    credentials = request.get_json()
    username, password = credentials['username'], credentials['password']
    user = db.session.query(User).filter(User.username == username).first()
    match = bcrypt.checkpw(password.encode('utf-8'), user.password or b'')
    if not match:
        return make_response('', 401)
    else:
        return app.make_response(("invalid credentials", 500))
'''
