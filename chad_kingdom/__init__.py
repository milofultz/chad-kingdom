from flask import Flask, redirect, url_for
from os import makedirs, remove, chmod, path
from sys import exit
from .jwt import is_logged_in



def create_app():
    app = Flask(__name__, instance_relative_config=True, template_folder='../templates', static_folder='../static')
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=path.join(app.instance_path, 'data.db')
    )
    
    # Set up instance directory
    try:
        makedirs(app.instance_path)
        print("Made new instance directory.")
    except OSError:
        print("Using preexisting instance directory.")
    
    # Set up database
    from . import db
    with app.app_context(): # context allows 'db' module to use 'current_app' module
        db.init()
        
        db.test()
    
    # Import modules + init
    from . import login
    app.register_blueprint(login.bp, url_prefix='/login')
    
    from . import game
    app.register_blueprint(game.bp, url_prefix='/game')
    
    # Home route for app
    @app.get("/")
    def home():
        if is_logged_in():
            return redirect(url_for('game.game'))
        else:
            return redirect(url_for('login.login'))

    return app
