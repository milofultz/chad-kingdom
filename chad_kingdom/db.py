from flask import current_app
import sqlite3
from os.path import exists
from functools import wraps
from datetime import datetime
from .gamedata import GameData


def dict_factory(cursor, row):
    """
    Used to convert row key/values into dicts.
    """
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def db_op(func):
    """
    A wrapper to simplify the writing of functions that operate on the database.

    Opens a connection to the database and performs the wrapped function within
    that context.

    If a function needs specialized use of the sqlite3 connection, don't use this
    decorator. Just custom-write the code in the function definition.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        with sqlite3.connect(current_app.config['DATABASE']) as conn:
            conn.row_factory = dict_factory
            result = func(conn, *args, **kwargs)
            conn.commit()
        return result
    return wrapper


def init():
    if not exists(current_app.config['DATABASE']):
        new_db()

        import bcrypt
        jick_pw = bcrypt.hashpw(b"jickpassword", bcrypt.gensalt())
        irondung_pw = bcrypt.hashpw(b"irondungpassword", bcrypt.gensalt())

        new_user("jick", jick_pw, "SCRUNG")
        new_user("irondung", irondung_pw, "bing bang")
    else:
        print(f"Using database: {current_app.config['DATABASE']}")


@db_op
def new_db(conn):
    cur = conn.cursor()
    with open('schema.sql', 'r') as f:
        cur.executescript(f.read())


@db_op
def test(conn):
    cur = conn.cursor()
    cur.execute("SELECT username, password, gamedata FROM users")
    for item in cur.fetchall():
        print(*item)


@db_op
def get_user(conn, username):
    cur = conn.cursor()
    cur.execute("SELECT * FROM users WHERE username = ? LIMIT 1", [username])
    user = cur.fetchone()
    return user


@db_op
def new_user(conn, username, password, character_name):
    """
    Generates new row in 'users' table, returns ID
    """
    #todo: do we need disallowed chars in password?
    #todo: also generate entry in characters table at the same time
    cur = conn.cursor()

    gd = GameData(character_name)
    cur.execute("INSERT INTO users (username, password, gamedata) VALUES (?, ?, ?)", (username, password, gd.serialize()))
    return cur.lastrowid


@db_op
def update_gamedata(conn, username, gamedata):
    cur = conn.cursor()

    cur.execute("""
        UPDATE users
        SET gamedata = ?
        WHERE username = ?;
    """, (gamedata, username))
    return True

