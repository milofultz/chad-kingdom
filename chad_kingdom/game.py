from flask import Blueprint, redirect, render_template
from . import db
from .jwt import token_required
from .gamedata import GameData
from .location import setup_gamemap


bp = Blueprint('game', __name__, template_folder='templates', static_folder='static')
gamemap = setup_gamemap()


@bp.route('/')
@token_required
def game(username: str):
    user = db.get_user(username)
    gamedata = GameData.deserialize(user["gamedata"])
    location = gamemap.get_location(gamedata.location)
    return render_template(f"locations/{location.page}")
    # user = db.get_user(username)
    # gamedata = GameData.deserialize(user["gamedata"])
    # # if you are in an encounter, load up the encounter
    # # if they are not in an encounter, loads up the page associated with the player's current location
    # return user["gamedata"]


@bp.route('/go/<location>')
@token_required
def go(username: str, location: str):
    user = db.get_user(username)
    gamedata = GameData.deserialize(user["gamedata"])
    connections = gamemap.get_connections(gamedata.location)
    if location in connections:
        gamedata.location = location
        gamedata_dump = gamedata.serialize()
        db.update_gamedata(username, gamedata_dump)
        print('location: ', location)
        return redirect('/game')
    else:
        return f"ERROR: {location} is not a connection with {gamedata.location}"


@bp.route('/do')
@token_required
def do(username: str):
    return "do"


@bp.route('/buy')
@token_required
def buy(username: str):
    return "buy"
