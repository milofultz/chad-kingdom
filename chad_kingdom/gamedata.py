from dataclasses import dataclass, field, asdict
import json


@dataclass
class GameData:
    character_name: str
    level: int = 69
    location: str = "world"
    money: int = 0
    inventory: dict = field(default_factory=dict)
    
    def adjust_inventory(self, item_type: str, quantity: int = 1):
        """
        Adds the quantity of a certain item to the inventory. Allows adding and removing items.
        
        Does not allow negative quantities of an item.
        
        """
        if item_type in self.inventory:
            self.inventory[item_type] += quantity
        else:
            self.inventory[item_type] = quantity
        
        if self.inventory[item_type] <= 0:
            del self.inventory[item_type]
    
    def serialize(self):
        return json.dumps(asdict(self))
    
    @staticmethod
    def deserialize(json_string: str):
        return GameData(**json.loads(json_string))

    
"""
Game data uses:

- store current game state for player
- easily serialized and deserialized

"""

if __name__ == '__main__':
    
    gd = GameData("Peter")
    gd.adjust_inventory("sword", 1)
    gd.adjust_inventory("potion", 2)
    gd.adjust_inventory("potion", -1)
    gd.adjust_inventory("creatine", 10)
    gd.adjust_inventory("creatine", -5)
    json_string = gd.serialize()
    print(json_string)
    new_gd = GameData.deserialize(json_string)
    json_string2 = new_gd.serialize()
    
    if json_string == json_string2:
        print("SUCCESS: serialized and deserialized without data loss")
    else:
        print("FAILURE: serialized and deserialized versions are different")