import os
from dotenv import load_dotenv
load_dotenv()

from functools import wraps
import time
from typing import Callable

from flask import make_response, redirect, request
import jwt

from enum import Enum

class LoginState(Enum):
    VALID_TOKEN = 0
    NO_TOKEN = 1
    INVALID_TOKEN = 2


def make_jwt_token(username: str):
        now = time.time() # Unix timestamp in seconds
        eight_hours_from_now = now + 60 * 60 * 8 # seconds * minutes * hours
        payload = {
            "iat": now,
            "exp": eight_hours_from_now,
            "username": username,
        }
        return jwt.encode(payload, os.getenv("JWT_SECRET_KEY"), algorithm="HS256")


def get_authorization():
    if 'Authorization' not in request.cookies:
        return LoginState.NO_TOKEN, {}
    
    token = request.cookies.get('Authorization').removeprefix('Bearer ')
    try:
        data = jwt.decode(token, os.getenv("JWT_SECRET_KEY"), algorithms=["HS256"])
        return LoginState.VALID_TOKEN, data
    except:
        return LoginState.INVALID_TOKEN, {}


def token_required(func: Callable):
    @wraps(func)
    def decorated(*args, **kwargs):
        state, data = get_authorization()
        
        if state == LoginState.VALID_TOKEN:
            username = data['username']
            return func(username, *args, **kwargs)
            
        elif state == LoginState.INVALID_TOKEN:
            return make_response('Invalid credentials', 401)
        
        elif state == LoginState.NO_TOKEN:
            return redirect('/')
        
    return decorated


def is_logged_in():
    state, data = get_authorization()
    if state == LoginState.VALID_TOKEN:
        return True
    else:
        return False

