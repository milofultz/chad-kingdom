

class Location:
    def __init__(self, name, html_url, *connections):
        self.name = name
        self.page = html_url
        self.connections = connections


class Map:
    def __init__(self):
        self.locations = {}

    def add_location(self, name, html_url, *connections):
        self.locations[name] = Location(name, html_url, *connections)

    def get_connections(self, name):
        return self.locations[name].connections
    
    def get_location(self, name):
        return self.locations[name]


def setup_gamemap():
    gamemap = Map()
    gamemap.add_location("world", "world.html", "house", "store", "arena")
    gamemap.add_location("house", "house.html", "world")
    gamemap.add_location("store", "store.html", "world")
    gamemap.add_location("arena", "arena.html", "world")
    return gamemap

