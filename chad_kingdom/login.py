from flask import Blueprint, make_response, redirect, render_template, request
import bcrypt

from .jwt import make_jwt_token
from . import db


bp = Blueprint('login', __name__, template_folder='templates', static_folder='static')


@bp.route('/')
def login():
    return render_template("login.html")


@bp.post("/auth")
def auth():
    username, password = request.form['username'], request.form['password']
    user = db.get_user(username)
    match = bcrypt.checkpw(password.encode('utf-8'), user["password"] or b'')
    if not match:
        return make_response('', 401)
    else:
        token = make_jwt_token(username)
        resp = make_response(redirect('/game'))
        resp.set_cookie('Authorization', value = f'Bearer {token}', httponly = True)
        return resp
