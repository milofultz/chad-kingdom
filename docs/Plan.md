# GAME

* Roll a character
* Locations
  * Store
  * Home
  * Arena
* Items (both exploration and battle)
* Attacks and spells (battle only)


## Modes

* (Exploration)
* Battle


## Exploration Mode


## Battle Mode

* Turn-based

# AUTH

* Username and password
* User session that maintains logged in state
* DB table of users
* Tokens

https://realpython.com/token-based-authentication-with-flask/
pip install pyjwt==1.4.2 : https://pyjwt.readthedocs.io/en/latest/
https://www.bacancytechnology.com/blog/flask-jwt-authentication

```python
# Create Flask app

# create route for home
  # load login page

# Create route for login
  # if username and password match
    # show success
  # else
    # show error
```

---

Endpoints for

* `/` = play the game if logged in else login
* `perform-action`

nav has

* play game
* player information area
* logout

Game state is held in the player's database entry

* Name
* Character Stats
* Location
* Equipment Stats
* Encounter Data
  * In battle
  * HP

---

## Encounter

* Turn based, a la KOL, Pokemon, JRPG
* Enemies do random things, damage, no AI
* Storage
  * Enemy and their states
    * Monster Type
    * Level
    * HP
* Monster Table
  * Name
  * Picture URL
  * Flavor Text
  * Attack info
    * Weapon
    * Band of attack number
* Table of Monster Instances from the Monster table
* User has encounter ID
* Encounter table holds
  * Monster type
  * Monster HP
  * Monster level
  * Any other attributes

* Monster Nerd

---

# NEXT STEPS

* Make database table for users
* Create dummy users for Mason and Milo (at startup)
* Determine game state shape and character info shape
  * Player
  * Items
  * Join tables (inventory of player for many items)
* Determine battle scenario and decision trees
* Make auth for users work (not as necessary for now)

